import discord
from discord.ext.commands import Bot
from discord.ext import commands
import asyncio
import time
"""
   Template Created by Camerin Figueroa
   Installation Instructions:
     Ubuntu:
       Pending
   
"""

class Bot(discord.Client):
    def __init__(self): # Initializes when bot is created
        super().__init__()
        self.player = None
        self.song = None
        self.voice = None
    async def on_ready(self): # Initializes when bot is run. Tells terminal that bot is online, bot's client id and name.
        print("Bot Online!")
        print("Name: {}".format(self.user.name))
        print("ID: {}".format(self.user.id))
#Main Course of script. Receives message and processes information
    async def on_message(self,message): # When bot receives a message
        if message.content.startswith('?pig'): # Replies to message
            await self.send_message(message.channel, "POG betches")
        elif message.content.startswith('?ping'): # Replies to message
            await self.send_message(message.channel, "Please don't abuse that and dos me...")
        elif message.content.startswith('?leave'): # kills script
            await self.send_message(message.channel, "Ok Byte...")
            import sys
            sys.exit(1)
        elif message.content.startswith('hello') or message.content.startswith("Hello"): # Replies to anyone who says hello
            await self.send_message(message.channel, "STFU you a** FU %s" % (message.author.name,))
        elif message.content.startswith('?help'): # Prints out Help(needs to be updated)
            await self.send_message(message.channel, "Help:")
            await self.send_message(message.channel, "_hello: Don't say that")
            await self.send_message(message.channel, "_?rick: Pickled RICK")
            await self.send_message(message.channel, "_?pig: sends back a pog")
            await self.send_message(message.channel, "_?ping: complains")
            await self.send_message(message.channel, "_?leave: Leaves discord")
            await self.send_message(message.channel, "_?help: helps you...")
            await self.send_message(message.channel, "_?stop: Stops any song that is playing...")
        elif message.content.startswith('?stop'): # Stops a song if playing
            if self.player != None:
                if self.player.is_playing() == True:
                    self.player.stop()
                    await self.send_message(message.channel, "Bot has stopped playing %s" % (self.song))
                    self.song = None
                else:
                    pass
            else:
                await self.send_message(message.channel, "Bot is not in voice channel...")
        elif message.content.startswith('?rick'): # Starts a song "rick roll"
            #channel = self.get_channel(message.author.voice.voice_channel)
            channel = message.author.voice.voice_channel
            print("Joining %s" % (channel))
            self.song = 'Rick Roll'
            if self.voice != None:
                if self.is_voice_connected(message.server):
                    await self.voice.disconnect()
            self.voice = await self.join_voice_channel(channel)
            self.player = await self.voice.create_ytdl_player('https://www.youtube.com/watch?v=dQw4w9WgXcQ')
            self.player.start()
            
        else: # If someone puts ?anything the bot will send a message to their channel
            if message.content != "" and message.content[0] == "?":
                await self.send_message(message.channel, "Please type the help command to receive help...")

bot = Bot()
bot.run("token")